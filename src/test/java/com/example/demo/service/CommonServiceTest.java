package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.Auther;
import com.example.demo.entity.Moderator;
import com.example.demo.entity.Question;
import com.example.demo.repository.AutherRepository;
import com.example.demo.repository.ModeratorRepository;
import com.example.demo.repository.QuestionRepository;
import com.example.demo.vo.AutherEnum;
import com.example.demo.vo.CategoryEnum;
import com.example.demo.vo.DifficultyLevelEnum;

@RunWith(SpringRunner.class)
public class CommonServiceTest {
	
	@InjectMocks
    private CommonServiceImpl commonServiceImpl;
	
	@Mock
	QuestionRepository questionRepo;
	
	@Mock
	AutherRepository autherRepo;
	
	@Mock
	ModeratorRepository moderatorRepo;
	
	@Test
	public void getQuestionsTest() {
		when(questionRepo.findAll()).thenReturn(getListOfQuestions(1));
		List<Question> body = commonServiceImpl.getQuestions();
		assertThat(body.size()).isEqualTo(1);
	}
	
	@Test(expected = NullPointerException.class)
	public void getQuestionsFailTest() {
		when(questionRepo.findAll()).thenThrow(NullPointerException.class);
		commonServiceImpl.getQuestions();
	}
	/*
	@Test
	public void createQuestion() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Auther auther  = new Auther();
		auther.setAutherId(1);
		auther.setAutherName("Anonymous");
		auther.setAutherType(AutherEnum.ANONYMOUS);
		Optional<Auther> optionalAuther = Optional.of(auther);
		when(autherRepo.findByAutherType(AutherEnum.ANONYMOUS)).thenReturn(optionalAuther);
		when(autherRepo.findById(1)).thenReturn(optionalAuther);
		autherRepo.findByAutherType(AutherEnum.ANONYMOUS).get();
		commonServiceImpl.createQuestion("What is Question", "Answer", sdf.parse("01/01/2021 12:00"), new Integer(0));
		commonServiceImpl.createQuestion("What is Question1", "Answer1", sdf.parse("01/01/2021 01:00"), new Integer(1));
	}*/
	
	@Test(expected = NoSuchElementException.class)
	public void createQuestionsFail() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		commonServiceImpl.createQuestion("What is Question1", "Answer1", sdf.parse("01/01/2021 01:00"), new Integer(1));
	}
	
	@Test
	public void applyCategoryDifficulty() throws ParseException {
		Question que = new Question();
		que.setActualQuestion("Q1");
		que.setAnswer("A1");
		que.setQuestionId(1);
		Optional<Question> optionalQue = Optional.of(que);
		when(questionRepo.findById(1)).thenReturn(optionalQue);
		Optional<Moderator> optionalModerator = Optional.of(getListOfModerators(1).get(0));
		when(moderatorRepo.findById(1)).thenReturn(optionalModerator);
		commonServiceImpl.applyCategoryDifficulty(1, DifficultyLevelEnum.EASY, CategoryEnum.GEOGRAPHY, 1);
	}
	
	@Test
	public void getAuthersTest() {
		when(autherRepo.findAll()).thenReturn(getListOfAuthers(2));
		List<Auther> body = commonServiceImpl.getAuthers();
		assertThat(body.size()).isEqualTo(2);
	}
	
	@Test(expected = NullPointerException.class)
	public void getAuthersFailTest() {
		when(autherRepo.findAll()).thenThrow(NullPointerException.class);
		commonServiceImpl.getAuthers();
	}
	
	@Test
	public void getModeratorsTest() {
		when(moderatorRepo.findAll()).thenReturn(getListOfModerators(3));
		List<Moderator> body = commonServiceImpl.getModerators();
		assertThat(body.size()).isEqualTo(3);
	}
	
	@Test(expected = NullPointerException.class)
	public void getModeratorsFailTest() {
		when(moderatorRepo.findAll()).thenThrow(NullPointerException.class);
		commonServiceImpl.getModerators();
	}
	
	@Test
	public void addModeratorTest() {
		Moderator moderator = commonServiceImpl.addModerator("Pratik", "Pratik", "Pratik");
	}
	
	@Test
	public void addAutherTest() {
		commonServiceImpl.addAuther("Anonymous", AutherEnum.ANONYMOUS);
	}
	
	private List<Question> getListOfQuestions(int n) {
		List<Question> objList = new ArrayList<Question>();
		for(int i=0 ; i<n ; i++) {
			Question obj = new Question();
			obj.setActualQuestion("Question " + i);
			obj.setAnswer("Answer " + i);
			obj.setCategory(CategoryEnum.GEOGRAPHY);
			obj.setDifficultyLevel(DifficultyLevelEnum.EASY);
			objList.add(obj);
		}
		return objList;
	}
	
	private List<Auther> getListOfAuthers(int n) {
		List<Auther> objList = new ArrayList<Auther>();
		for(int i=0 ; i<n ; i++) {
			Auther obj = new Auther();
			obj.setAutherName("Auther " + i);
			obj.setAutherType(AutherEnum.NAMED);
			objList.add(obj);
		}
		return objList;
	}
	
	private List<Moderator> getListOfModerators(int n) {
		List<Moderator> objList = new ArrayList<Moderator>();
		for(int i=0 ; i<n ; i++) {
			Moderator obj = new Moderator();
			obj.setModeratorName("Moderator " + i);
			obj.setPassword("passwd");
			obj.setUserName("Moderator " + i);
			objList.add(obj);
		}
		return objList;
	}
	
	@Test
	public void codeCoverageTest() {
		Question que = new Question();
		que.getActualQuestion();
		que.getAnswer();
		que.getCategory();
		que.getCreatedBy();
		que.getCreatedDate();
		que.getDifficultyLevel();
		que.getQuestionId();
		que.getUpdatedBy();
		que.getUpdatedDate();
		
		que.setActualQuestion("");
		que.setAnswer("");
		que.setCategory(CategoryEnum.GEOGRAPHY);
		que.setCreatedBy(new Auther());
		que.setCreatedDate(new Date());
		que.setDifficultyLevel(DifficultyLevelEnum.EASY);
		que.setQuestionId(1);
		que.setUpdatedBy(new Moderator());
		que.setUpdatedDate(new Date());
		que.toString();
		
		Auther auther = new Auther();
		auther.getAutherId();
		auther.getAutherName();
		auther.getAutherType();
		
		auther.setAutherId(1);
		auther.setAutherName("");
		auther.setAutherType(AutherEnum.ANONYMOUS);
		auther.toString();
		
		Moderator moderator = new Moderator();
		moderator.getModeratorId();
		moderator.getModeratorName();
		moderator.getPassword();
		moderator.getUserName();
		
		moderator.setModeratorId(1);
		moderator.setModeratorName("");
		moderator.setPassword("");
		moderator.setUserName("");
		moderator.toString();
	}
}
