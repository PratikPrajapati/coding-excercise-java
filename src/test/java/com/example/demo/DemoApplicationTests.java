package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entity.Question;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void homeResponse() {
		restTemplate.getRestTemplate().getInterceptors().add(
				  new BasicAuthorizationInterceptor("Pratik", "Pratik"));
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("Spring is here!");
	}
	
	@Test
	public void questionsResponse() {
		restTemplate.getRestTemplate().getInterceptors().add(
				  new BasicAuthorizationInterceptor("Pratik", "Pratik"));
		List<Question> body = this.restTemplate.getForObject("/questions", List.class);
		assertThat(body.size()).isEqualTo(0);
	}

}
