package com.example.demo.graphquery;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.demo.entity.Auther;
import com.example.demo.entity.Moderator;
import com.example.demo.entity.Question;
import com.example.demo.service.CommonService;
import com.example.demo.vo.CategoryEnum;
import com.example.demo.vo.DifficultyLevelEnum;

@Component
public class QuestionQuery implements GraphQLQueryResolver, GraphQLMutationResolver{

	@Autowired
	private CommonService commonService;
	
	public List<Question> getQuestions() {
		return commonService.getQuestions();
	}
	
	public List<Auther> autherByName(final String autherName) {
		return commonService.getAutherByName(autherName);
	}
	

	public Question createQuestion(final String strQuestion, final String answer, final Date createDate, final Integer autherId) {
		return commonService.createQuestion(strQuestion, answer, createDate, autherId);
	}

	public Question applyCategoryDifficulty(final Integer questionId, final DifficultyLevelEnum difficultyLevel, final CategoryEnum category, Integer moderatorId) {
		return commonService.applyCategoryDifficulty(questionId, difficultyLevel, category, moderatorId);
	}
	
	public List<Auther> getAuthers() {
		return commonService.getAuthers();
	}

	public List<Moderator> getModerators() {
		return commonService.getModerators();
	}

}
