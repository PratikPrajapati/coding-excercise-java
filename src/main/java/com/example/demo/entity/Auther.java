package com.example.demo.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.example.demo.vo.AutherEnum;

@Entity
public class Auther {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "autherId", nullable = false)
	private Integer autherId;
	@Column(name = "autherType", nullable = false)
	private AutherEnum autherType;
	@Column(name = "autherName", nullable = false)
	private String autherName;
	@OneToMany(mappedBy = "createdBy", fetch = FetchType.EAGER)
	private Set<Question> questions;
	
	public Integer getAutherId() {
		return autherId;
	}
	public void setAutherId(Integer autherId) {
		this.autherId = autherId;
	}
	public AutherEnum getAutherType() {
		return autherType;
	}
	public void setAutherType(AutherEnum autherType) {
		this.autherType = autherType;
	}
	public String getAutherName() {
		return autherName;
	}
	public void setAutherName(String autherName) {
		this.autherName = autherName;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Auther [autherId=").append(autherId)
			.append(", autherType=").append(autherType)
			.append(", autherName=").append(autherName).append("]");
		return builder.toString();
	}
}
