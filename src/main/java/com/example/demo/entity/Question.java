package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.example.demo.vo.CategoryEnum;
import com.example.demo.vo.DifficultyLevelEnum;

@Entity
public class Question {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "questionId", nullable = false)
	private Integer questionId;
	@Column(name = "actualQuestion", nullable = false)
	private String actualQuestion;
	@Column(name = "category")
	private CategoryEnum category;
	@Column(name = "difficultyLevel")
	private DifficultyLevelEnum difficultyLevel;
	@Column(name = "answer", nullable=false)
	private String answer;
	@Column(name = "createDate", nullable=false)
	private Date createdDate;
	@ManyToOne(fetch = FetchType.EAGER)
	private Auther createdBy;
	@Column(name = "updatedDate")
	private Date updatedDate;
	@ManyToOne(fetch = FetchType.EAGER)
	private Moderator updatedBy;
	
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public String getActualQuestion() {
		return actualQuestion;
	}
	public void setActualQuestion(String actualQuestion) {
		this.actualQuestion = actualQuestion;
	}
	public CategoryEnum getCategory() {
		return category;
	}
	public void setCategory(CategoryEnum category) {
		this.category = category;
	}
	public DifficultyLevelEnum getDifficultyLevel() {
		return difficultyLevel;
	}
	public void setDifficultyLevel(DifficultyLevelEnum difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Auther getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Auther createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Moderator getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Moderator updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Question [questionId=").append(questionId)
			.append(", actualQuestion=").append(actualQuestion)
			.append(", category=").append(category)
			.append(", difficultyLevel=").append(difficultyLevel)
			.append(", answer=").append(answer)
			.append(", createdDate=").append(createdDate)
			.append(", createdBy=").append(createdBy)
			.append(", updatedDate=").append(updatedDate)
			.append(", updatedBy=").append(updatedBy)
			.append("]");
		return builder.toString();
	}
}
