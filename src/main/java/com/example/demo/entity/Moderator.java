package com.example.demo.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Moderator {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "moderatorId", nullable = false)
	private Integer moderatorId;
	@Column(name = "userName", nullable = false)
	private String userName;
	@Column(name = "password", nullable = false)
	private String password;
	@Column(name = "moderatorName", nullable = false)
	private String moderatorName;
	@OneToMany(mappedBy = "updatedBy", fetch = FetchType.EAGER)
	private Set<Question> questions;
	
	public Integer getModeratorId() {
		return moderatorId;
	}
	public void setModeratorId(Integer moderatorId) {
		this.moderatorId = moderatorId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getModeratorName() {
		return moderatorName;
	}
	public void setModeratorName(String moderatorName) {
		this.moderatorName = moderatorName;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Moderator [moderatorId=").append(moderatorId)
			.append(", userName=").append(userName)
			.append(", moderatorName=").append(moderatorName)
			.append("]");
		return builder.toString();
	}

	
}
