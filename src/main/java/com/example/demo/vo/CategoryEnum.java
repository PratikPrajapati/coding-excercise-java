package com.example.demo.vo;

public enum CategoryEnum {
	GEOGRAPHY, SCIENCE, POLITICS, LITERATURE, OTHERS
}
