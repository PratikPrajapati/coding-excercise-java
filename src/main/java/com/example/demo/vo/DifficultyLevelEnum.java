package com.example.demo.vo;

public enum DifficultyLevelEnum {
	EASY, MEDIUM, HARD
}
