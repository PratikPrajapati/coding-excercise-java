package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Auther;
import com.example.demo.vo.AutherEnum;

@Repository
public interface AutherRepository extends JpaRepository<Auther, Integer> {
	
	List<Auther> findByAutherType(AutherEnum autherType);
	
	List<Auther> findByAutherNameIsContaining(String autherName);
	
}
