package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Auther;
import com.example.demo.entity.Moderator;
import com.example.demo.entity.Question;
import com.example.demo.service.CommonServiceImpl;
import com.example.demo.vo.AutherEnum;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

@SpringBootApplication
@RestController
public class DemoApplication {

	private static final Logger logger = LoggerFactory.getLogger(DemoApplication.class);
	private static final String CLASSNAME = CommonServiceImpl.class.getSimpleName();
	private static final String STARTED = "Started...";
	private static final String ENDED = "Ended...";

	@Autowired
	CommonServiceImpl commonService;

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@GetMapping("/questions")
	List<Question> questions() {
		return commonService.getQuestions();
	}

	@PostConstruct
	public void addModerators() {
		String methodName = " [ PostConstruct ] [ addModerators ] ";
		try {
			logger.info(CLASSNAME + methodName + STARTED);
			Moderator moderator = commonService.addModerator("Pratik", "Pratik", "Pratik Prajapati");
			logger.info(CLASSNAME + methodName + " Moderator : " + moderator);
			Auther auther = commonService.addAuther("Chetan Bhagat", AutherEnum.NAMED);
			logger.info(CLASSNAME + methodName + " Auther : " + auther);
			auther = commonService.addAuther("Anonymous", AutherEnum.ANONYMOUS);
			logger.info(CLASSNAME + methodName + " Auther : " + auther);
			logger.info(CLASSNAME + methodName + ENDED);
		}catch(Exception e) {
			e.printStackTrace();
			logger.info(CLASSNAME + methodName + e.getMessage());
		}
	}

	@Bean
	public GraphQLScalarType dateScalar() {
		return GraphQLScalarType.newScalar()
				.name("DateTime")
				.description("Java 8 DateTime as scalar.")
				.coercing(new Coercing<Date, String>() {
					@Override
					public String serialize(final Object input) {
						if (input instanceof Date) {
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm");
							return sdf.format((Date)input);
						} else {
							throw new CoercingSerializeException("Expected a Date object.");
						}
					}
					@Override
					public Date parseValue(final Object input) {
						try {
							if (input instanceof StringValue) {
								SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm");
								return sdf.parse(((StringValue)input).getValue());
							} else {
								throw new CoercingParseValueException("Expected a String");
							}
						} catch (DateTimeParseException | ParseException e) {
							throw new CoercingParseValueException(String.format("Not a valid date: '%s'.", input), e);
						}
					}
					@Override
					public Date parseLiteral(final Object input) {
						if (input instanceof StringValue) {
							try {
								SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm");
								logger.info(CLASSNAME + " parseLiteral " + " input : " + input);
								return sdf.parse(((StringValue)input).getValue());
							} catch (DateTimeParseException | ParseException e) {
								throw new CoercingParseLiteralException(e);
							}
						} else {
							throw new CoercingParseLiteralException("Expected a StringValue.");
						}
					}
				}).build();
	}
}