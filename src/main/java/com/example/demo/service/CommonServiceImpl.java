package com.example.demo.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Auther;
import com.example.demo.entity.Moderator;
import com.example.demo.entity.Question;
import com.example.demo.repository.AutherRepository;
import com.example.demo.repository.ModeratorRepository;
import com.example.demo.repository.QuestionRepository;
import com.example.demo.vo.AutherEnum;
import com.example.demo.vo.CategoryEnum;
import com.example.demo.vo.DifficultyLevelEnum;

@Service
public class CommonServiceImpl implements CommonService {

	private static final Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);
	private static final String CLASSNAME = CommonServiceImpl.class.getSimpleName();
	private static final String STARTED = "Started...";
	private static final String STARTED_WITH = "Started With : ";
	private static final String ENDED = "Ended...";
	private static final String EXCEPTION_OCCURED = "Exception Occured : ";
	
	@Autowired
	QuestionRepository questionRepo;
	
	@Autowired
	AutherRepository autherRepo;
	
	@Autowired
	ModeratorRepository moderatorRepo;
	
	@Override
	@Transactional
	public List<Question> getQuestions() {
		String methodName = " [ getQuestions ] ";
		logger.info(CLASSNAME + methodName + STARTED);
		List<Question> questionList = null;
		try {
			questionList = questionRepo.findAll();
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return questionList;
	}
	
	@Override
	@Transactional
	public Question createQuestion(String strQuestion, String answer, Date createDate, Integer autherId) {
		String methodName = " [ createQuestion ] ";
		logger.info(CLASSNAME + methodName + STARTED_WITH + answer);
		Question response = null;
		try {
			Question question = new Question();
			question.setActualQuestion(strQuestion);
			question.setAnswer(answer);
			question.setCreatedDate(createDate);
			if(null==autherId || autherId == 0) {
				question.setCreatedBy(autherRepo.findByAutherType(AutherEnum.ANONYMOUS).get(0));
			}else {
				question.setCreatedBy(autherRepo.findById(autherId).get());
			}
			response = questionRepo.save(question);
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return response;
	}
	
	public Question applyCategoryDifficulty(Integer questionId, DifficultyLevelEnum difficultyLevel, CategoryEnum category, Integer moderatorId) {
		String methodName = " [ applyCategoryDifficulty ] ";
		logger.info(CLASSNAME + methodName + STARTED_WITH + questionId + " : " + difficultyLevel + " : " + category);
		Optional<Question> response = null;
		Question responseQuestion = null;
		try {
			response = questionRepo.findById(questionId);
			if(response.isPresent()) {
				responseQuestion = response.get();
				responseQuestion.setDifficultyLevel(difficultyLevel);
				responseQuestion.setCategory(category);
				responseQuestion.setUpdatedDate(new Date());
				responseQuestion.setUpdatedBy(moderatorRepo.findById(moderatorId).get());
				responseQuestion = questionRepo.save(responseQuestion);
			}else {
				logger.info(CLASSNAME + methodName + "Question With ID Not Found");
				//response validation error message
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return responseQuestion;
	}
	
	@Override
	@Transactional
	public List<Auther> getAuthers() {
		String methodName = " [ getAuthers ] ";
		logger.info(CLASSNAME + methodName + STARTED);
		List<Auther> autherList = null;
		try {
			autherList = autherRepo.findAll();
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return autherList;
	}
	
	@Override
	@Transactional
	public List<Auther> getAutherByName(String autherName) {
		String methodName = " [ getAuthers ] ";
		logger.info(CLASSNAME + methodName + STARTED);
		List<Auther> auther = null;
		try {
			auther = autherRepo.findByAutherNameIsContaining(autherName);
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return auther;
	}
	
	@Override
	@Transactional
	public List<Moderator> getModerators() {
		String methodName = " [ getModerators ] ";
		logger.info(CLASSNAME + methodName + STARTED);
		List<Moderator> moderatorList = null;
		try {
			moderatorList = moderatorRepo.findAll();
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return moderatorList;
	}
	
	@Override
	@Transactional
	public Moderator addModerator(String userName, String password, String moderatorName) {
		String methodName = " [ addModerator ] ";
		logger.info(CLASSNAME + methodName + STARTED);
		Moderator moderator = new Moderator();
		try {
			moderator.setUserName(userName);
			moderator.setPassword(password);
			moderator.setModeratorName(moderatorName);
			moderator = moderatorRepo.save(moderator);
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return moderator;
	}
	
	@Override
	@Transactional
	public Auther addAuther(String autherName, AutherEnum autherType) {
		String methodName = " [ addAuther ] ";
		logger.info(CLASSNAME + methodName + STARTED);
		Auther auther = new Auther();
		try {
			auther.setAutherName(autherName);
			auther.setAutherType(autherType);
			auther = autherRepo.save(auther);
		}catch(Exception e) {
			e.printStackTrace();
			logger.error(CLASSNAME + methodName + EXCEPTION_OCCURED + e.getMessage());
			throw e;
		}
		logger.info(CLASSNAME + methodName + ENDED);
		return auther;
	}
}
