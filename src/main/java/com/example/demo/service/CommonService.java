package com.example.demo.service;

import java.util.Date;
import java.util.List;

import com.example.demo.entity.Auther;
import com.example.demo.entity.Moderator;
import com.example.demo.entity.Question;
import com.example.demo.vo.AutherEnum;
import com.example.demo.vo.CategoryEnum;
import com.example.demo.vo.DifficultyLevelEnum;

public interface CommonService {
	
	public List<Question> getQuestions();
	public List<Auther> getAutherByName(String autherName);
	public Question createQuestion(String question, String answer, Date createDate, Integer autherId);
	public Question applyCategoryDifficulty(Integer questionId, DifficultyLevelEnum difficultyLevel, CategoryEnum category, Integer moderatorId);
	public List<Auther> getAuthers();
	public List<Moderator> getModerators();
	public Moderator addModerator(String userName, String password, String moderatorName);
	public Auther addAuther(String autherName, AutherEnum autherType);

}
